import React from "react";

function SortDropdown({ handleSort }) {
  return (
    <select className="dropDown w-[200px]" onChange={handleSort}>
      <option value="">Sort By</option>
      <option value="Area (ASC)">Area (ASC)</option>
      <option value="Area (DSC)">Area (DSC)</option>
      <option value="Population (ASC)">Population (ASC)</option>
      <option value="Population (DSC)">Population (DSC)</option>
    </select>
  );
}

export default SortDropdown;
