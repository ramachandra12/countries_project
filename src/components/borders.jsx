import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import restCountries from "../assets/countries.json";

function Borders({ value }) {
  const [country, setCountry] = useState(null);
  useEffect(() => {
    restCountries.forEach((element) => {
      if (element.cca3 === value) {
        setCountry(element.name.common);
      }
    });
  }, [value]);

  return (
    <>
      <Link to={`/country/${country}`}>
        <p className="back px-5  text-center justify-self-center ">{country}</p>
      </Link>
    </>
  );
}

export default Borders;
