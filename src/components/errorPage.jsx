function Error() {
  return (
    <>
      <h1 className="h-[88vh] font-bold text-3xl  text-center">
        No such countries found
      </h1>
    </>
  );
}

export default Error;
