import { Link } from "react-router-dom";

function Card({ value }) {
  return (
    <Link to={`/country/${value.name.common}`}>
      <div className="card">
        <img className="img" src={value.flags.png} alt="" />
        <div className="data">
          <h1 className="heading">{value.name.common}</h1>
          <p>
            <span className="details">Area :</span> {value.area}
          </p>
          <p>
            <span className="details">Population :</span>{" "}
            {value.population.toLocaleString()}
          </p>
          <p>
            <span className="details">Region :</span> {value.region}
          </p>
          <p>
            <span className="details">SubRegion :</span> {value.subregion}
          </p>
          <p>
            <span className="details">Capital:</span> {value.capital}
          </p>
        </div>
      </div>
    </Link>
  );
}

export default Card;
