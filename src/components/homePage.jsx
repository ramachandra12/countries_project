import { useState } from "react";
import SearchBar from "./search";
import Body from "./body";

function Main({ countriesData }) {
  const [input, setInput] = useState("");
  const [region, setRegion] = useState("");
  const [subregion, setSubRegion] = useState("");
  const [sort, setSort] = useState("");
  return (
    <div className="max-w-[1600px] mx-auto min-h-[88vh] bg-[hsl(0,0%,98%)}">
      <SearchBar
        countriesData={countriesData}
        setInput={setInput}
        setRegion={setRegion}
        setSubRegion={setSubRegion}
        setSort={setSort}
      />
      <Body
        countriesData={countriesData}
        input={input}
        region={region}
        subregion={subregion}
        sort={sort}
      />
    </div>
  );
}

export default Main;
