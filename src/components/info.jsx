function Info({ country }) {
  function Currency() {
    for (const key in country["currencies"]) {
      return country["currencies"][key].name;
    }
  }
  function Languages() {
    let a = [];
    for (const key in country) {
      if (key == "languages") {
        for (const lag in country[key]) {
          a.push(country[key][lag]);
        }
      }
    }
    return a;
  }

  return (
    <div className=" flex gap-20">
      <div>
        <h1 className="font-bold text-3xl"> {country.name.common}</h1>

        <div className="pt-10">
          <p className="info">
            <span className="details">Native Name :</span>{" "}
            {country.translations.nld.common}
          </p>
          <p className="info">
            <span className="details">Population :</span>{" "}
            {country.population.toLocaleString()}
          </p>
          <p className="info">
            <span className="details">Region :</span> {country.region}
          </p>
          <p className="info">
            <span className="details">SubRegion :</span> {country.subregion}
          </p>
          <p className="info">
            <span className="details">Capital:</span> {country.capital}
          </p>
        </div>
      </div>
      <div className="  pt-20">
        <p className="info">
          <span className="details">Top Level Domain :</span> {country.tld}
        </p>
        <p className="info">
          <span className="details">Currencies :</span> {Currency()}
        </p>
        <p className="info">
          <span className="details">Languages :</span> {Languages() + ""}
        </p>
      </div>
    </div>
  );
}

export default Info;
