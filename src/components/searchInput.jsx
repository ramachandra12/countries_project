
import React from "react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function SearchInput({ handleInput }) {
  return (
    <label className="label" htmlFor="search">
      <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>{" "}
      <input
        className="input-field"
        type="text"
        placeholder="search for country "
        id="search"
        onChange={handleInput}
        autoComplete="off"
      />
    </label>
  );
}

export default SearchInput;
