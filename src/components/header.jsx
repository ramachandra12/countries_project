import { useContext } from "react";
import themeContext from "./themeContext";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Header() {
  const { theme, setTheme } = useContext(themeContext);

  function changeTheme() {
    setTheme(theme === "dark" ? "light" : "dark");
  }

  return (
    <div className="header-section">
      <div className="header py-5">
        <h1 className="font-bold text-2xl">Where in the world?</h1>
        <button className="mode" onClick={changeTheme}>
          {theme === "dark" ? (
            <FontAwesomeIcon icon={faSun} />
          ) : (
            <FontAwesomeIcon icon={faMoon} />
          )}{" "}
          &nbsp;&nbsp;
          {theme === "dark" ? "Light Mode" : "Dark Mode"}
        </button>
      </div>
    </div>
  );
}

export default Header;
