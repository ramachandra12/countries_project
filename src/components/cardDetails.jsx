import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Info from "./info";
import Borders from "./borders";
import restCountries from "../assets/countries.json";

export default function CardDetails() {
  const { id } = useParams();
  const [country, setCountry] = useState(null);

  useEffect(() => {
    restCountries.forEach((element) => {
      if (element.name.common === id) {
        setCountry(element);
      }
    });
  }, [id]);

  return (
    country && (
      <div className="max-width-[1600px] ml-[10rem] min-h-[88vh]">
        <Link to={"/"}>
          <h1 className="back w-[8rem] h-[2rem]  text-xl mb-20 ">
            <FontAwesomeIcon className="px-3" icon={faArrowLeft} /> Back
          </h1>
        </Link>
        <div className="flex gap-52">
          <img className="w-[500px] h-[350px]" src={country.flags.png} alt="" />
          <div className="">
            <Info country={country} />
            <div className="info flex mt-10 flex-wrap gap-5">
              <span className={`details ${!country.borders && `hidden`}`}>
                Border Countries :
              </span>
              {country.borders &&
                country.borders.map((element) => {
                  return <Borders value={element} key={element} />;
                })}
            </div>
          </div>
        </div>
      </div>
    )
  );
}
