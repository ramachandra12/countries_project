import Card from "./card";

function Body({ countriesData, input, region, subregion, sort }) {
  const filteredCountries = countriesData.filter(
    (element) =>
      element.name.common.toLowerCase().includes(input.toLowerCase()) &&
      element.region.toLowerCase().includes(region.toLowerCase()) &&
      String(element.subregion).toLowerCase().includes(subregion.toLowerCase())
  );
  switch (sort) {
    case "Area (ASC)":
      filteredCountries.sort((a, b) => a.area - b.area);
      break;
    case "Area (DSC)":
      filteredCountries.sort((a, b) => b.area - a.area);
      break;
    case "Population (DSC)":
      filteredCountries.sort((a, b) => b.population - a.population);
      break;
    case "Population (ASC)":
      filteredCountries.sort((a, b) => a.population - b.population);
      break;
  }

  return (
    <div className="cardContainer">
      {filteredCountries.length > 0 ? (
        filteredCountries.map((element, index) => (
          <Card key={index} value={element} />
        ))
      ) : (
        <h1 className=" h-[79vh] font-bold text-3xl  mx-auto">
          No such countries found
        </h1>
      )}
    </div>
  );
}

export default Body;
