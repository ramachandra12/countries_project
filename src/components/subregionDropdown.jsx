import React from "react";

function SubregionDropdown({ subRegions, handleSubregion }) {
  return (
    <select className="dropDown" name="subregion" onChange={handleSubregion}>
      <option value="">Filter by SubRegion</option>
      {subRegions.map((element, index) => {
        return <option key={index} value={`${element}`}>{element}</option>;
      })}
    </select>
  );
}

export default SubregionDropdown;
