
import React from "react";

function RegionDropdown({ regions, handleRegion }) {
  return (
    <select className="dropDown" name="region" onChange={handleRegion}>
      <option value="">Filter by Region</option>
      {regions.map((element, index) => {
        return <option key={index} value={`${element}`}>{element}</option>;
      })}
    </select>
  );
}

export default RegionDropdown;
