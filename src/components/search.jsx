import React, { useState } from "react";
import SearchInput from "./searchInput";
import SortDropdown from "./sortDropdown";
import SubregionDropdown from "./subregionDropdown";
import RegionDropdown from "./regionDropDown";

function SearchBar({
  countriesData,
  setInput,
  setRegion,
  setSubRegion,
  setSort,
}) {
  const [getSubRegions, setGetSubRegions] = useState([]);

  function handleRegion(event) {
    setRegion(event.target.value);
    let a = [];
    countriesData.forEach((element) => {
      if (
        element.region === event.target.value &&
        !a.includes(element.subregion)
      ) {
        a.push(element.subregion);
      }
    });
    setGetSubRegions([...a]);
  }

  function handleInput(event) {
    setInput(event.target.value);
  }

  function handleSort(event) {
    setSort(event.target.value);
  }

  function handleSubregion(event) {
    setSubRegion(event.target.value);
  }
  const regions = countriesData.reduce((acc, data) => {
    if (!acc.includes(data.region)) {
      acc.push(data.region);
    }
    return acc;
  }, []);

  return (
    <div className="search_bar">
      <SearchInput handleInput={handleInput} />
      <SortDropdown handleSort={handleSort} />
      <SubregionDropdown
        subRegions={getSubRegions}
        handleSubregion={handleSubregion}
      />
      <RegionDropdown regions={regions} handleRegion={handleRegion} />
    </div>
  );
}

export default SearchBar;
