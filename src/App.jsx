import { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/header";
import themeContext from "./components/themeContext";
import Main from "./components/homePage";
import CardDetails from "./components/cardDetails";
import Error from "./components/errorPage";
import restCountries from "./assets/countries.json"
import "./App.css";

function App() {
   const [theme, setTheme] = useState("light");
  
  return (
    <div className={theme}>
       
        <themeContext.Provider value={{ theme, setTheme }}>
        <Header />
      </themeContext.Provider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Main countriesData={restCountries}  />}>
            {" "}
          </Route>
          <Route path="/country/:id" element={<CardDetails />}></Route>
          <Route path="*" element={<Error />}></Route>
        </Routes>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
